package me.icyrelic.com.Commands;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FakeCommand implements CommandExecutor {
	
	LegendaryMessages plugin;
	public FakeCommand(LegendaryMessages instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("fake")) {
			
			if(sender.hasPermission("LegendaryMessages.Fake")){
				
				if(sender instanceof Player){
					if (args.length == 1){
						Player p = (Player) sender;
						  String name = "";
						  if(plugin.getConfig().getBoolean("UseNicknames")){
							  name = p.getDisplayName();
						  }else{
							  name = p.getName();
						  }
			        	 if (args[0].equalsIgnoreCase("join")){
			        		 plugin.getServer().broadcastMessage(plugin.getConfig().getString("Returning_Join_Message.Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("%player_name%", name));
			        	 }
			        	 if (args[0].equalsIgnoreCase("quit")){
			        		 plugin.getServer().broadcastMessage(plugin.getConfig().getString("Quit_Message.Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("%player_name%", name));		
			        	 }
					}else{
						sender.sendMessage(ChatColor.RED + "Usage: /fake <join | quit>");
					}
				}else{
					sender.sendMessage("You are not a player so how can you join or quit?");
				}
			}

			
		}
		return true;
	}
	
}
