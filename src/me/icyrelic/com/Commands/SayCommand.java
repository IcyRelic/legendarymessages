package me.icyrelic.com.Commands;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SayCommand implements CommandExecutor {
	
	LegendaryMessages plugin;
	public SayCommand(LegendaryMessages instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("say")) {
			
			if(sender.hasPermission("LegendaryMessages.Say")){
				if(args.length >= 1){
					plugin.getServer().broadcastMessage(plugin.getConfig().getString("Say_Prefix").replaceAll("(&([a-f0-9]))", "\u00A7$2") + " " +getFinalArg(args, 0));
				}else{
					sender.sendMessage(ChatColor.RED + "Usage: /say <message>");
				}
				
			}else{
				sender.sendMessage(ChatColor.RED + "You Dont Have Permission");
			}
			
		}
		return true;
	}
	
	public static String getFinalArg(final String[] args, final int start)
	{
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++)
		{
			if (i != start)
			{
				bldr.append(" ");
			}
			bldr.append(args[i]);
		}
		return bldr.toString();
	}

}
