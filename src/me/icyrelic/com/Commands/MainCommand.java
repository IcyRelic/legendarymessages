package me.icyrelic.com.Commands;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MainCommand implements CommandExecutor {
	
	LegendaryMessages plugin;
	public MainCommand(LegendaryMessages instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("legendarymessages")) {
			
		  String noPerm = (ChatColor.RED + "You Dont Have Permission!");
		  
		  if (args.length >= 1){
			  
        	 if (args[0].equalsIgnoreCase("reload")){
        		 
        		 if(sender.hasPermission("LegendaryMessages.Reload")){
        			 
        			 plugin.reloadConfig();
        			 
        			 sender.sendMessage(plugin.prefix + ChatColor.WHITE + "Config Reloaded!");
        			 
        		 }else{
        			 
        			 sender.sendMessage(noPerm);
        			 
        		 }
        		 
             }  
        	 		         	 
		  }else{
			  
			  sender.sendMessage(ChatColor.GREEN + "LegendaryMessages v"+plugin.getDescription().getVersion());
			  
		  }
		  
		}
		return true;
	}
	


}
