package me.icyrelic.com.Commands;

import java.util.Date;
import java.util.UUID;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoCommand implements CommandExecutor {
	
	LegendaryMessages plugin;
	public InfoCommand(LegendaryMessages instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		 String noPerm = (ChatColor.RED + "You Dont Have Permission!");

		if (cmd.getName().equalsIgnoreCase("playerinfo")) {
			
			if(plugin.useMySQL){
				
				
				
					if (args.length == 1){
			        	 if (args[0].equalsIgnoreCase("newestplayer")){
			        		 if(sender.hasPermission("LegendaryMessages.Info.NewestPlayer")){
									sender.sendMessage(plugin.prefix + ChatColor.GREEN+"Newest Player: "+ChatColor.GRAY+plugin.mysql.newestPlayer());
								}else{
									sender.sendMessage(noPerm);
								}
			        		 
			        	 }
			        	 else if (args[0].equalsIgnoreCase("totalplayers")){
			        		 if(sender.hasPermission("LegendaryMessages.Info.TotalPlayers")){
									sender.sendMessage(plugin.prefix + ChatColor.GREEN+"Total Players: "+ChatColor.GRAY+plugin.mysql.totalPlayers());
								}else{
									sender.sendMessage(noPerm);
								}
			        		 
			        	 }else{
			        		 
			        		 
			        		 if(sender.hasPermission("LegendaryMessages.Info.Others")){
			        			 
			        			 
			        			 String name = args[0].toLowerCase();
			        			 UUID uid = null;
			        			 if(plugin.mysql.getUID(name) == null){
			        				 sender.sendMessage(plugin.prefix+"No information for player '"+name+"' can be found");
			        				 return true;
			        			 }else{
			        				 uid = plugin.mysql.getUID(name);
			        			 }
			        			 
			        			 Player p = (Player) plugin.getServer().getPlayer(uid);
									sender.sendMessage(ChatColor.GRAY + "--------------- "+ChatColor.GREEN+"Player Information"+ChatColor.GRAY+" ---------------");
								sender.sendMessage(ChatColor.GREEN+"Player Name: " +ChatColor.GRAY+ name);
								if(plugin.mysql.checkNewPlayer(uid, p.getName())){
									sender.sendMessage(ChatColor.RED + "No Other Information Available For This User");
								}else{
									sender.sendMessage(ChatColor.GREEN+"Alias Names: " +ChatColor.GRAY+ plugin.mysql.getAliasNames(uid));
									sender.sendMessage(ChatColor.GREEN+"First Seen: " +ChatColor.GRAY+ plugin.mysql.getDate(uid) + " at " + plugin.mysql.getTime(uid));
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"Last Seen: " +ChatColor.GRAY+ "Online Now!");
									}else{
										sender.sendMessage(ChatColor.GREEN+"Last Seen: " +ChatColor.GRAY+ plugin.mysql.lastDate(uid) + " at " + plugin.mysql.lastTime(uid));
									}
									
									String days = " Days ";
									String hours = " Hours ";
									String minutes = " Minutes ";
									String seconds = " Seconds ";
									
									
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"Time Played: " +ChatColor.GRAY+ plugin.mysql.getExactPlayed(uid, days, hours, minutes, seconds, plugin.logins.get(name),new Date()));
									}else{
										sender.sendMessage(ChatColor.GREEN+"Time Played: " +ChatColor.GRAY+ plugin.mysql.getPlayed(uid, days, hours, minutes, seconds));
									}
									//health
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										
										sender.sendMessage(ChatColor.GREEN+"Health: " +ChatColor.GRAY+ p.getHealth()+"/20");
									}else{
										sender.sendMessage(ChatColor.GREEN+"Health: " +ChatColor.GRAY+ plugin.mysql.health(uid));
									}
									//hunger
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"Hunger: " +ChatColor.GRAY+ p.getFoodLevel()+"/20");
									}else{
										sender.sendMessage(ChatColor.GREEN+"Hunger: " +ChatColor.GRAY+ plugin.mysql.hunger(uid));
									}
									//location
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"Location: " +ChatColor.GRAY+ "X: "+p.getLocation().getBlockX()+", Y: "+p.getLocation().getBlockY()+", Z: "+p.getLocation().getBlockZ());
									}else{
										sender.sendMessage(ChatColor.GREEN+"Location: " +ChatColor.GRAY+ plugin.mysql.location(uid));
									}
									//item in hand
									if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
										sender.sendMessage(ChatColor.GREEN+"Item In Hand: " +ChatColor.GRAY+ p.getItemInHand().getType());
									}else{
										sender.sendMessage(ChatColor.GREEN+"Item In Hand: " +ChatColor.GRAY+ plugin.mysql.itemInHand(uid));
									}
									
									
									
									
									sender.sendMessage(ChatColor.GREEN+"IP Address: " +ChatColor.GRAY+ plugin.mysql.getIP(uid));
									sender.sendMessage(ChatColor.GREEN+"Player Number: " +ChatColor.GRAY+ plugin.mysql.getPlayerNumber(uid));
									sender.sendMessage(ChatColor.GREEN+"UniqueID: " +ChatColor.GRAY+ uid);
								}
								sender.sendMessage(ChatColor.GRAY+"-----------------------------------------------");
								}else{
									sender.sendMessage(noPerm);
								}
			        		 
			        		 
								
			        	 }
					}else{
						
						if(sender instanceof Player){
							if(sender.hasPermission("LegendaryMessages.Info.Own")){
								// just /info
							Player p = (Player) sender;
							UUID uid = p.getUniqueId();
							sender.sendMessage(ChatColor.GRAY + "--------------- "+ChatColor.GREEN+"Player Information"+ChatColor.GRAY+" ---------------");
							sender.sendMessage(ChatColor.GREEN+"Player Name: " +ChatColor.GRAY+ sender.getName().toLowerCase());
							sender.sendMessage(ChatColor.GREEN+"Alias Names: " +ChatColor.GRAY+ plugin.mysql.getAliasNames(uid));
							sender.sendMessage(ChatColor.GREEN+"First Seen: " +ChatColor.GRAY+ plugin.mysql.getDate(uid) + " at " + plugin.mysql.getTime(uid));
							if(plugin.getServer().getOfflinePlayer(uid).isOnline()){
								sender.sendMessage(ChatColor.GREEN+"Last Seen: " +ChatColor.GRAY+ "Online Now!");
							}else{
								sender.sendMessage(ChatColor.GREEN+"Last Seen: " +ChatColor.GRAY+ plugin.mysql.lastDate(uid) + " at " + plugin.mysql.lastTime(uid));
							}
							
							
							String days = " Days ";
							String hours = " Hours ";
							String minutes = " Minutes ";
							String seconds = " Seconds ";
							
							sender.sendMessage(ChatColor.GREEN+"Time Played: " +ChatColor.GRAY+ plugin.mysql.getExactPlayed(uid, days, hours, minutes, seconds, plugin.logins.get(p.getName().toLowerCase()),new Date()));
							
							sender.sendMessage(ChatColor.GREEN+"Health: " +ChatColor.GRAY+ p.getHealth()+"/20");
							sender.sendMessage(ChatColor.GREEN+"Hunger: " +ChatColor.GRAY+ p.getFoodLevel()+"/20");
							sender.sendMessage(ChatColor.GREEN+"Location: " +ChatColor.GRAY+ "X: "+p.getLocation().getBlockX()+", Y: "+p.getLocation().getBlockY()+", Z: "+p.getLocation().getBlockZ());
							sender.sendMessage(ChatColor.GREEN+"Item In Hand: " +ChatColor.GRAY+ p.getItemInHand().getType());
							
							
							
							sender.sendMessage(ChatColor.GREEN+"IP Address: " +ChatColor.GRAY+ plugin.mysql.getIP(uid));
							sender.sendMessage(ChatColor.GREEN+"Player Number: " +ChatColor.GRAY+ plugin.mysql.getPlayerNumber(uid));
							sender.sendMessage(ChatColor.GREEN+"UniqueID: " +ChatColor.GRAY+ uid);
							sender.sendMessage(ChatColor.GRAY+"-----------------------------------------------");
							}else{
								sender.sendMessage(noPerm);
							}
						}else{
							sender.sendMessage(plugin.prefix + ChatColor.RED + "This command is for players only");
						}
						

						
					}
			}else{
				sender.sendMessage(plugin.prefix + "This command is only available if LegendaryMessages uses MySQL storage type!");
			}
			
		}
		return true;
	}
	

}
