package me.icyrelic.com.Listeners;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLogin implements Listener{
	
	LegendaryMessages plugin;
	public PlayerLogin(LegendaryMessages instance) {

		plugin = instance;

		}
	
	ChatColor bold = ChatColor.BOLD;
    ChatColor italic = ChatColor.ITALIC;
    ChatColor underline = ChatColor.UNDERLINE;
    ChatColor magic = ChatColor.MAGIC;
    ChatColor strike = ChatColor.STRIKETHROUGH;
    ChatColor reset = ChatColor.RESET;
	
	@EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLogin(PlayerLoginEvent event)
    {
		
		if(event.getKickMessage().equals("You are not white-listed on this server!")){
			event.setKickMessage(plugin.getConfig().getString("Whitelist_Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("&r", reset+"").replace("&l", bold+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+""));
			
		}
        
    }
	

}
