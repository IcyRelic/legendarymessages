package me.icyrelic.com.Listeners;

import java.util.Date;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.MetadataValue;

public class PlayerQuit implements Listener{
	
	LegendaryMessages plugin;
	public PlayerQuit(LegendaryMessages instance) {

		plugin = instance;

		}
	ChatColor bold = ChatColor.BOLD;
    ChatColor italic = ChatColor.ITALIC;
    ChatColor underline = ChatColor.UNDERLINE;
    ChatColor magic = ChatColor.MAGIC;
    ChatColor strike = ChatColor.STRIKETHROUGH;
    ChatColor reset = ChatColor.RESET; 
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent event){
		event.setQuitMessage("");
		String name = "";
		    if(plugin.getConfig().getBoolean("UseNicknames")){
			  name = event.getPlayer().getDisplayName();
		  }else{
			  name = event.getPlayer().getName();
		  }
		
		    
		    
		if(plugin.getConfig().getBoolean("Quit_Message.Enabled")){
			
			if(!isVanished(event.getPlayer())){
				String quit = plugin.getConfig().getString("Quit_Message.Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("&r", reset+"").replace("&l", bold+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"").replace("%player_name%", name);
			
				  String [] split = quit.split("/n");
				  
				  int y = 0;
				  while(y < split.length){
					  plugin.getServer().broadcastMessage(split[y]);
					  y++;
				  }
			
			}else{
				  if(plugin.vanished.containsKey(event.getPlayer().getName())){
					  plugin.vanished.remove(event.getPlayer().getName());
				  }
				  plugin.vanished.put(event.getPlayer().getName(), true);
			  }
			
		}
		
		
		if(plugin.useMySQL){
			plugin.mysql.delOnlinePlayer();
			Date logout = new Date();
			Date login = plugin.logins.get(event.getPlayer().getName().toLowerCase());
			Player p = event.getPlayer();
			plugin.mysql.updateStats(p.getUniqueId(), p.getFoodLevel()+"/20", p.getHealth()+"/20", p.getLocation().getBlockX(), p.getLocation().getBlockY(), p.getLocation().getBlockZ(), p.getItemInHand().getType().toString(), login, logout);
			plugin.logins.remove(event.getPlayer().getName());
			
		}
		  
		  
		  
		
	}
	
	
	
	public boolean isVanished(Player p) { 
		for(MetadataValue value : p.getMetadata("vanished")) { 
			if (value.asBoolean()) { 
				return true;
			} 
		} return false; 
	}

}
