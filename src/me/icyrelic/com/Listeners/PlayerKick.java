package me.icyrelic.com.Listeners;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

public class PlayerKick implements Listener {
	
	LegendaryMessages plugin;
	public PlayerKick(LegendaryMessages instance) {

		plugin = instance;

		}
	ChatColor bold = ChatColor.BOLD;
    ChatColor italic = ChatColor.ITALIC;
    ChatColor underline = ChatColor.UNDERLINE;
    ChatColor magic = ChatColor.MAGIC;
    ChatColor strike = ChatColor.STRIKETHROUGH;
    ChatColor reset = ChatColor.RESET;
	
	@EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerKick(PlayerKickEvent e)
    {
		
		String name = "";
	    if(plugin.getConfig().getBoolean("UseNicknames")){
		  name = e.getPlayer().getDisplayName();
	  }else{
		  name = e.getPlayer().getName();
	  }
		e.setLeaveMessage("");
		if(plugin.getConfig().getBoolean("Kick_Message.Enabled")){
			
			
			String kick = plugin.getConfig().getString("Kick_Message.Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("&r", reset+"").replace("&l", bold+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"").replace("%player_name%", name);
			
			  String [] split = kick.split("/n");
			  
			  int y = 0;
			  while(y < split.length){
				  plugin.getServer().broadcastMessage(split[y]);
				  y++;
			  }
			
		}
        
    }
	

}
