package me.icyrelic.com.Listeners;

import java.util.List;
import java.util.Random;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftSkeleton;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeath implements Listener {
	
	LegendaryMessages plugin;
	public PlayerDeath(LegendaryMessages instance) {
		plugin = instance;
	}
	
	static ChatColor bold = ChatColor.BOLD;
	static ChatColor italic = ChatColor.ITALIC;
    static ChatColor underline = ChatColor.UNDERLINE;
    static ChatColor magic = ChatColor.MAGIC;
    static ChatColor strike = ChatColor.STRIKETHROUGH;
    static ChatColor reset = ChatColor.RESET;
	
	@EventHandler
	public void death(PlayerDeathEvent e){
		e.setDeathMessage(null);
		Player p = e.getEntity();
		ConsoleCommandSender console = plugin.getServer().getConsoleSender();
		
		if(!plugin.getConfig().getBoolean("Death_Message.Enabled"))
			return;
			
		EntityDamageEvent damageEvent = p.getLastDamageCause();
		if(!(damageEvent instanceof EntityDamageByEntityEvent) && !(damageEvent == null)){
			String de = damageEvent.getCause().toString();
			
			try{
				broadcastOther(p, de, damageEvent.getCause().toString());
			} catch (NullPointerException exception){
				console.sendMessage(plugin.prefix + ChatColor.RED + "ERROR: Please report the following messages to the LegendaryMessages dev page");
				console.sendMessage("-- Other --");
				console.sendMessage(damageEvent.getCause().toString());
				console.sendMessage(de);
			}
			
		}else if(!(damageEvent == null)){
			Entity damager = ((EntityDamageByEntityEvent)damageEvent).getDamager();
			if(damager instanceof Arrow){
				 Projectile arrow = (Arrow) damager;
				 
				 try{
					 if(arrow.getShooter() instanceof Player){
						 broadcastPlayer(p, (Player) arrow.getShooter());
					 }if(arrow.getShooter() instanceof CraftSkeleton){
						 broadcastMob(p, EntityType.SKELETON);
					 } else {
						 broadcastMob(p, EntityType.valueOf(arrow.getShooter().toString()));
					 }
				 } catch (NullPointerException exception){
					console.sendMessage(plugin.prefix + ChatColor.RED + "ERROR: Please report the following messages to the LegendaryMessages dev page");
					console.sendMessage("-- Arrow ("+arrow.getShooter().toString()+")--");
					console.sendMessage(damageEvent.getCause().toString());
					console.sendMessage(arrow.getShooter().toString());
				}
				 
			}else{
				try{
					broadcastMob(p, damager.getType());
				 } catch (NullPointerException exception){
					
					plugin.getServer().getConsoleSender().sendMessage(plugin.prefix + ChatColor.RED + "ERROR: Please report the following messages to the LegendaryMessages dev page");
					console.sendMessage("-- Mob --");
					console.sendMessage(damageEvent.getCause().toString());
					console.sendMessage(damager.getType()+"");
				}
				
			}
			
			if(damager instanceof Player){
				try{
					broadcastPlayer(p, p.getKiller());
				 } catch (NullPointerException exception){
					console.sendMessage(plugin.prefix + ChatColor.RED + "ERROR: Please report the following messages to the LegendaryMessages dev page");
					console.sendMessage("-- Player --");
					console.sendMessage(damageEvent.getCause().toString());
				}
				
				
			}
		}else{
			console.sendMessage(p.getName()+" has died but LegendaryMessages does not know why! Please report this error on the LegendaryMessages bukkit page!");
		}
	}		
	
	private void broadcastMob(Player p, EntityType type) throws NullPointerException{
		
			broadcast(getRandMsg(type.name(), "Mobs")
					.replace("%mob%", type.name())
					.replace("%killed%", p.getName())
					.replace("%world%", p.getWorld().getName()));
			
	}
	
	
	private void broadcastPlayer(Player p, Player killer) throws NullPointerException{
		
		String item = p.getKiller().getItemInHand().getType().toString();
		
		broadcast(getMsg("Player")
				.replace("%killed%", p.getName())
				.replace("%killer%", killer.getName())
				.replace("%world%", p.getWorld().getName())
				.replace("%weapon%", item));
		
	}
	
	
	private void broadcastOther(Player p, String type, String cause) throws NullPointerException{
		
		broadcast(getRandMsg(type, "Other")
				.replace("%killed%", p.getName())
				.replace("%world%", p.getWorld().getName())
				.replace("%cause%", cause));
		
	}
	
    
	private void broadcast(String msg) throws NullPointerException{
		Bukkit.getServer().broadcastMessage(msg);
	}
	
	
    private String addColor(String msg){
		
		return msg
				.replaceAll("(&([a-f0-9]))", "\u00A7$2")
				.replace("&r", reset+"")
				.replace("&l", bold+"")
				.replace("&o", italic+"")
				.replace("&n", underline+"")
				.replace("&k", magic+"")
				.replace("&m", strike+"");
		
	}
	
		
	private String getRandMsg(String type, String cat) throws NullPointerException{
		List<?> Messages;
		String MSG;
		Random rand = new Random();
		
		Messages = plugin.getConfig().getList("Death_Message.Messages."+cat+"."+type);
		MSG = Messages.get(rand.nextInt(Messages.size())).toString();
		
		return addColor(MSG);
	}
	
		
	private String getMsg(String type){
		return addColor(plugin.getConfig().getString("Death_Message.Messages."+type));
	}
	
	
	
}