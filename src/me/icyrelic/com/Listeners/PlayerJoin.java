package me.icyrelic.com.Listeners;


import java.util.Date;
import java.util.UUID;

import me.icyrelic.com.LegendaryMessages;
import me.icyrelic.com.Data.PlayersFile;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class PlayerJoin implements Listener{
	
	LegendaryMessages plugin;
	public PlayerJoin(LegendaryMessages instance) {

		plugin = instance;

		}
	
	ChatColor bold = ChatColor.BOLD;
    ChatColor italic = ChatColor.ITALIC;
    ChatColor underline = ChatColor.UNDERLINE;
    ChatColor magic = ChatColor.MAGIC;
    ChatColor strike = ChatColor.STRIKETHROUGH;
    ChatColor reset = ChatColor.RESET;

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){

 
		  event.setJoinMessage("");
		  Player p = event.getPlayer();
		  String name = "";
		  
		  
		  
		  if(plugin.getConfig().getBoolean("UseNicknames")){
			  name = p.getDisplayName();
		  }else{
			  name = p.getName();
		  }
		  
		  
		  playerjoin(p, name);

	  }
	
	
	public boolean newPlayer(Player p){
		 UUID UID = p.getUniqueId();
		if(!plugin.useMySQL){
			return PlayersFile.newPlayerCheck(p);
		}else{
			return plugin.mysql.checkNewPlayer(UID, p.getName());
		}
		
	}
	
	public void addPlayer(Player p, int x){
		if(!plugin.useMySQL){
			PlayersFile.addNewPlayer(p);
		}else{
			plugin.mysql.addPlayer(p, x);
		}
	}
	
	public int getTotalPlayers(){
		if(!plugin.useMySQL){
			return PlayersFile.getTotalPlayers();
		}else{
			return plugin.mysql.totalPlayers();
		}
	}
	
	public void addMySQLPlayer(Player p){
		if(plugin.useMySQL){
			plugin.logins.put(p.getName().toLowerCase(), new Date());
			plugin.mysql.addOnlinePlayer();
			if(!plugin.mysql.checkAlias(p.getUniqueId(), p.getName())){
				plugin.mysql.addAlias(p.getUniqueId(), ", "+p.getName());
			}
		}
	}
	
	public void playerjoin(Player p, String name){
		
		if(newPlayer(p)){
			
			addPlayer(p, getTotalPlayers());
			addMySQLPlayer(p);
			
			  if(plugin.getConfig().getBoolean("New_Join_Message.Enabled")){
				  if(!plugin.vanished.containsKey(p.getName())){
					  String new_join = plugin.getConfig().getString("New_Join_Message.Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("%player_name%", name).replace("%total_players%", ""+getTotalPlayers()).replace("&r", reset+"").replace("&l", bold+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"");
					  String [] split = new_join.split("/n");
					  
					  int y = 0;
					  while(y < split.length){
						  plugin.getServer().broadcastMessage(split[y]);
						  y++;
					  }
					  
					 
				  }else{
					  if(plugin.vanished.containsKey(p.getName())){
						  plugin.vanished.remove(p.getName());
					  }
					  p.sendMessage(ChatColor.GREEN + "You have silently joined");
				  }
			  }
			  

			  
			  
			  
			
		}else{
			addMySQLPlayer(p);
			
			if(plugin.getConfig().getBoolean("Returning_Join_Message.Enabled")){
				 if(!plugin.vanished.containsKey(p.getName())){
					 
					  String join = plugin.getConfig().getString("Returning_Join_Message.Message").replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("%player_name%", name).replace("&r", reset+"").replace("&l", bold+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"");
					  String [] split = join.split("/n");
					  int y = 0;
					  
	    	    			
					  
					  
					  while(y < split.length){
						  plugin.getServer().broadcastMessage(split[y]);
						  y++;
					  }
					 
				 }else{
					  if(plugin.vanished.containsKey(p.getName())){
						  plugin.vanished.remove(p.getName());
					  }
					  p.sendMessage(ChatColor.GREEN + "You have silently joined");
				  }
				
			}
			
		}
		
		
		if(plugin.getConfig().getBoolean("MOTD.Enabled")){
			
			  String motd = plugin.getConfig().getString("MOTD.Message").replace("%playername%", name).replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("&r", reset+"").replace("&l", bold+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"");
			  String [] split = motd.split("/n");
			  
			  int y = 0;
			  while(y < split.length){
				  p.sendMessage(split[y]);
				  y++;
			  }
			
		}
		
	}
	
			  
	
}
