package me.icyrelic.com;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import me.icyrelic.com.Commands.*;
import me.icyrelic.com.Data.MySQL;
import me.icyrelic.com.Data.PlayersFile;
import me.icyrelic.com.Data.Updater;
import me.icyrelic.com.Data.Updater.UpdateResult;
import me.icyrelic.com.Data.Updater.UpdateType;
import me.icyrelic.com.Listeners.*;
import me.icyrelic.com.Metrics.Metrics;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;


public class LegendaryMessages extends JavaPlugin{
	
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryMessages" + ChatColor.WHITE + "] ");
	public HashMap<String, Date> logins = new HashMap<String, Date>();
	public HashMap<String, Boolean> vanished = new HashMap<String, Boolean>();
	public MySQL mysql = new MySQL(this);
	public boolean useMySQL = false;
	public String tbl_prefix;
	public String players_tbl;
	public String information_tbl;
	
	public void onEnable() {
		ConsoleCommandSender console = getServer().getConsoleSender();
		
		runUpdates(console);
		loadConfiguration();
		setupStorage(console);
		startMetrics();
		loadListeners();	
		loadCommands();
		startAutoMessages();
	}
	
	private void setupStorage(ConsoleCommandSender console){
		
		boolean use = getConfig().getBoolean("MySQL.Enabled");
		
		if(use){
			String host = getConfig().getString("MySQL.Host");
			String port = getConfig().getString("MySQL.Port");
			String user = getConfig().getString("MySQL.Username");
			String password = getConfig().getString("MySQL.Password");
			String db = getConfig().getString("MySQL.Database");
			console.sendMessage(prefix+"Trying To Connect To Database '"+db+"'");
			
			if(mysql.connect(host, port, db, user, password)){
				console.sendMessage(prefix+"StorageType: mysql");
				console.sendMessage(prefix+"Connected To Database '"+db+"'");
				useMySQL = true;
			}else{
				console.sendMessage(prefix+"Failed To Connect To Database '"+db+"'");
				console.sendMessage(prefix+"Defaulting to File");
				playerfilecheck();
				useMySQL = false;
			}
		}else{
			console.sendMessage(prefix+"StorageType: file");
			useMySQL = false;
			playerfilecheck();
		}
	}
	
	private void runUpdates(ConsoleCommandSender console){
		if(getConfig().getBoolean("AutoUpdate")){
			Updater check = new Updater(this, 39616, this.getFile(), UpdateType.NO_DOWNLOAD, true);
			
			if (check.getResult() == UpdateResult.UPDATE_AVAILABLE) {
			    console.sendMessage(prefix+"New Version Available! "+ check.getLatestName());
				Updater download = new Updater(this, 39616, this.getFile(), UpdateType.DEFAULT, true);
				
			    if(download.getResult() == UpdateResult.SUCCESS){
			    	console.sendMessage(prefix+"Successfully Updated Please Restart To Finalize");
			    }
			}else{
				console.sendMessage(prefix+"You are currently running the latest version of LegendaryMessages");
			}
		}
	}
	
	private void loadListeners(){
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerLogin(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerQuit(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDeath(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerKick(this), this);
	}
	
	private void loadCommands(){
		getCommand("legendarymessages").setExecutor(new MainCommand(this));
		getCommand("say").setExecutor(new SayCommand(this));
		getCommand("fake").setExecutor(new FakeCommand(this));
		getCommand("playerinfo").setExecutor(new InfoCommand(this));
	}

	public void onDisable(){
		ConsoleCommandSender console = getServer().getConsoleSender();
		
		console.sendMessage(prefix + "Shutting Down...");
		
		if(useMySQL){
			console.sendMessage(prefix + "Disconnecting From Database...");
			String db = getConfig().getString("MySQL.Database");
			mysql.shutdown(db);
		}
		

		
		console.sendMessage(prefix + "Disabled");
		
	}
	
	private void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
		tbl_prefix = getConfig().getString("MySQL.Table_Prefix");
		players_tbl = tbl_prefix+"players";
		information_tbl = tbl_prefix+"information";
	}
		
	private void playerfilecheck(){
		ConsoleCommandSender console = getServer().getConsoleSender();
		if(!PlayersFile.checkPlayersFile()){
			console.sendMessage(prefix+"players.yml is missing creating it!");
		}else{
			console.sendMessage(prefix+"Loaded players.yml!");
		}
	}
	
	private void startMetrics(){
		System.out.println("[LegendaryMessages] Trying to start Metrics");
		 try {
			    Metrics metrics = new Metrics(this);
			    metrics.start();
			} catch (IOException e) {
				System.out.println(prefix+"Failed to send metrics");
		 }
	}
	
	private void startAutoMessages(){
		
		if(getConfig().getBoolean("AutoMessages.Enabled") == true){
			Set<String> keys = getConfig().getConfigurationSection("AutoMessages.Messages").getKeys(false);
		    for (Iterator<String> it = keys.iterator(); it.hasNext(); ) {
		        String f = it.next();
		        
		        int x = getConfig().getInt("AutoMessages.Messages."+f+".interval");
				@SuppressWarnings("unused")
				BukkitTask task = new AutoMessages(this, f).runTaskTimer(this, 0, x);
		        
		        
		    }
			
		}
		
	}

	
	

}