package me.icyrelic.com.Data;

import java.io.File;
import java.io.IOException;

import me.icyrelic.com.LegendaryMessages;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PlayersFile {
	
	static LegendaryMessages plugin;
	public PlayersFile(LegendaryMessages instance) {

		plugin = instance;

		}
	
	public static boolean checkPlayersFile(){
		File PFile;
        PFile = new File("plugins/LegendaryMessages", "players.yml");
 	 if(PFile.exists()){
 		 return true;
 	 }else{
 		 FileConfiguration conf = YamlConfiguration.loadConfiguration(PFile);
 		 conf.set("Total_Players", 0);
 		 try {
			conf.save("plugins/LegendaryMessages/players.yml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		 return false;
 	 }
		
		
	}
	
	
	public static boolean newPlayerCheck(Player p){
		File PFile;
        PFile = new File("plugins/LegendaryMessages", "players.yml");
        
 	 if(PFile.exists()){
 		FileConfiguration conf = YamlConfiguration.loadConfiguration(PFile);
 		if(conf.getBoolean(p.getName()) == false){
 			return true;
 		}else{
 			return false;
 		}
 	 }else{
 		 return true;
 	 }
		
		
	}
	
	
	public static int getTotalPlayers(){
		File PFile;
        PFile = new File("plugins/LegendaryMessages", "players.yml");
        
 	 if(PFile.exists()){
 		FileConfiguration conf = YamlConfiguration.loadConfiguration(PFile);
 		int x = conf.getInt("Total_Players");
 		return x;
 	 }else{
 		return 0;
 	 }
		
		
	}
	
	public static void addNewPlayer(Player p){
		File PFile;
        PFile = new File("plugins/LegendaryMessages", "players.yml");
        
 	 if(PFile.exists()){
 		FileConfiguration conf = YamlConfiguration.loadConfiguration(PFile);
 		if(conf.getBoolean("Players."+p.getName()) == false){
 			conf.set(p.getName(), true);
 			int x = conf.getInt("Total_Players");
 			int y = x+1;
 			conf.set("Total_Players", y);
 			
 			
 	 		 try {
 				conf.save("plugins/LegendaryMessages" + File.separator + "players.yml");
 			} catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
 		}
 	 }
		
		
	}
	

}
