package me.icyrelic.com.Data;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


import me.icyrelic.com.LegendaryMessages;

import org.bukkit.entity.Player;

public class MySQL{
	
	static Connection con = null;
    Statement st = null;
    ResultSet rs = null;
	
    String db_version = "1";
    
	LegendaryMessages plugin;
	public MySQL(LegendaryMessages instance) {

		plugin = instance;

		}
	
	public boolean connect(String host, String port, String db, String user, String password){
		String prefix = ("[LegendaryMessages] ");
	    String url = "jdbc:mysql://"+ host + ":" + port + "/" + db;
		 try {
			con = DriverManager.getConnection(url, user, password);
			 st = con.createStatement();
			 try{
				 
				 System.out.println(prefix+"Loading Tables From Database '"+db+"'");
				 DatabaseMetaData md = con.getMetaData();
				 ResultSet rs = md.getTables(null, null, plugin.players_tbl, null);
				 if (!rs.next()) {
					System.out.println(prefix+"Creating "+plugin.players_tbl+" table");
					  Statement st = con.createStatement();
					  String table = 
							  "CREATE TABLE "+plugin.players_tbl+
					                   "(id INTEGER NOT NULL AUTO_INCREMENT, " +
					                   " username VARCHAR(255), " + 
					                   " date VARCHAR(255), " + 
					                   " time VARCHAR(255), " + 
					                   " player_number INTEGER(255), " + 
					                   " PRIMARY KEY ( id ))";
					  st.executeUpdate(table);
				 }else{
					System.out.println(prefix+"Loaded "+plugin.players_tbl+" table");
				 }
				 
				 rs = md.getTables(null, null, plugin.information_tbl+"", null);
				 if (!rs.next()) {
					System.out.println(prefix+"Creating "+plugin.information_tbl+" table");
					 String table1 = 
							  "CREATE TABLE "+plugin.information_tbl+
					                   "(id INTEGER NOT NULL AUTO_INCREMENT, " +
					                   " total_players VARCHAR(255), " + 
					                   " newest_player VARCHAR(255), " + 
					                   " PRIMARY KEY ( id ))";
					  st.executeUpdate(table1);
				 }else{
					System.out.println(prefix+"Loaded "+plugin.information_tbl+" table");
				 }
				 
				 

				  
				  
				  
				  
				  
				  updateDatabase();
				  addDefaultInfo();
				  return true;
			 }
			 catch(SQLException s){
				 return true;
			 }
		} catch (SQLException e) {
			return false;
		}
	}
	
	public void updateDatabase (){
		//Add new Columns that are in version 4.2+
		boolean didupdates = false;
		int x = 0;
		System.out.println("[LegendaryMessages] Checking for database updates");
		try {
			
			DatabaseMetaData md = con.getMetaData();
			ResultSet rs;
			
			
			
			
			rs = md.getColumns(null, null, plugin.information_tbl, "database_version");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE  "+plugin.information_tbl+" ADD  `database_version` VARCHAR( 255 ) NOT NULL FIRST");
				st.executeUpdate("UPDATE "+plugin.information_tbl+" SET database_version='0'");
				
			}
			
			
			
			//change date from date to varchar
			rs = md.getColumns(null, null, plugin.players_tbl, "date");
			if (rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" DROP `date`");
				st.executeUpdate("ALTER TABLE  "+plugin.players_tbl+" ADD  `date_joined` DATE NOT NULL AFTER  `username`");
				
			}
			
			//change time to time_joined
			rs = md.getColumns(null, null, plugin.players_tbl, "time");
			if (rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE  "+plugin.players_tbl+" CHANGE  `time`  `time_joined` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL");
				
			}
			
			
			//ip Column
			rs = md.getColumns(null, null, plugin.players_tbl, "ip_address");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `ip_address` VARCHAR( 255 ) NOT NULL");
				
			}
			
			
			//last_seen_date Column
			rs = md.getColumns(null, null, plugin.players_tbl, "last_seen_date");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE  "+plugin.players_tbl+" ADD  `last_seen_date` DATE NOT NULL");
				
			}
			
			//last_seen_time Column
			rs = md.getColumns(null, null, plugin.players_tbl, "last_seen_time");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `last_seen_time` VARCHAR( 255 ) NOT NULL");
				
			}
			
			
			
			//played days Column
			rs = md.getColumns(null, null, plugin.players_tbl, "played_days");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `played_days` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			//played hours Column
			rs = md.getColumns(null, null, plugin.players_tbl, "played_hours");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `played_hours` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			
			//played minutes Column
			rs = md.getColumns(null, null, plugin.players_tbl, "played_minutes");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `played_minutes` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			
			//played seconds Column
			rs = md.getColumns(null, null, plugin.players_tbl, "played_seconds");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `played_seconds` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			//played health Column
			rs = md.getColumns(null, null, plugin.players_tbl, "health");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `health` VARCHAR( 255 ) NOT NULL");
				
			}
			
			//played health Column
			rs = md.getColumns(null, null, plugin.players_tbl, "hunger");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `hunger` VARCHAR( 255 ) NOT NULL");
				
			}
			
			//played item in hand Column
			rs = md.getColumns(null, null, plugin.players_tbl, "item_in_hand");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `item_in_hand` VARCHAR( 255 ) NOT NULL");
				
			}
			
			//played location x Column
			rs = md.getColumns(null, null, plugin.players_tbl, "location_x");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `location_x` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			//played location y Column
			rs = md.getColumns(null, null, plugin.players_tbl, "location_y");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `location_y` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			
			//played location z Column
			rs = md.getColumns(null, null, plugin.players_tbl, "location_z");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `location_z` INT( 255 ) NOT NULL DEFAULT  '0'");
				
			}
			
			//played onlineplayers Column
			rs = md.getColumns(null, null, plugin.information_tbl, "online_players");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE  `"+plugin.information_tbl+"` ADD  `online_players` INT( 255 ) NOT NULL");
				
			}
			

			
			
			
			//played onlineplayers Column
			rs = md.getColumns(null, null, plugin.information_tbl, "max_players");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE  `"+plugin.information_tbl+"` ADD  `max_players` INT( 255 ) NOT NULL");
				
			}
			
			rs = md.getColumns(null, null, plugin.players_tbl, "UUID");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE "+plugin.players_tbl+" ADD `UUID` VARCHAR( 255 ) NOT NULL FIRST");
				
			}
			rs = md.getColumns(null, null, plugin.players_tbl, "alias_names");
			if (!rs.next()) {
				didupdates = true;
				x++;
				st.executeUpdate("ALTER TABLE `"+plugin.players_tbl+"` ADD `alias_names` LONGTEXT NOT NULL");
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		if(didupdates == true){
			System.out.println("[LegendaryMessages] "+x+" Updates Have Been Added To The Database");
		}else{
			System.out.println("[LegendaryMessages] No database updates available");
		}
		 
		
	}
	
	
	
	
	
	
	
	
	public void addDefaultInfo(){
		
		
        try {
           
        	
            rs = st.executeQuery("SELECT * FROM "+plugin.information_tbl+" WHERE id = '1'");
            
            if (!rs.next()) {
            	st.executeUpdate("INSERT INTO "+plugin.information_tbl+" (id, database_version ,total_players, newest_player, online_players, max_players) VALUES (NULL, '1.0' ,'0', '', '0', '0')");
            }
            
            st.executeUpdate("UPDATE "+plugin.information_tbl+" SET online_players = '"+plugin.getServer().getOnlinePlayers().size()+"' WHERE id = 1");
            
            st.executeUpdate("UPDATE "+plugin.information_tbl+" SET max_players = '"+plugin.getServer().getMaxPlayers()+"' WHERE id = 1");


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
	}
	
	public void addPlayer(Player p, int number){
		

        try {
           
            rs = st.executeQuery("SELECT * FROM "+plugin.players_tbl+" WHERE username = '"+p.getName()+"'");
            
            if (!rs.next()) {
            	Date dNow = new Date( );
                SimpleDateFormat ft = 
                new SimpleDateFormat ("yyyy-MM-dd");
                
                SimpleDateFormat ft1 = 
                new SimpleDateFormat ("h:mm aa");
                
                int x = number+1;
                
                String sql = "INSERT INTO "+plugin.players_tbl+" (`UUID`, `id` , `username` , `date_joined` , `time_joined` , `player_number` , `ip_address` , `last_seen_date` , `last_seen_time` , `played_days` , `played_hours` , `played_minutes` , `played_seconds` , `health` , `hunger` , `item_in_hand` , `location_x` , `location_y` , `location_z` , `alias_names`)"+
"VALUES ('"+p.getUniqueId()+"', NULL, '"+p.getName()+"', '"+ft.format(dNow)+"', '"+ft1.format(dNow)+"', '"+x+"', '"+p.getAddress().getAddress().getHostAddress()+"', '"+ft.format(dNow)+"', '"+ft1.format(dNow)+"', '0', '0', '0', '0', '"+p.getHealth()+"/20', '"+p.getFoodLevel()+"/20', '"+p.getItemInHand().getType()+"', '"+p.getLocation().getBlockX()+"', '"+p.getLocation().getBlockY()+"', '"+p.getLocation().getBlockZ()+"', '"+p.getName()+"');";

                st.executeUpdate("UPDATE "+plugin.information_tbl+" SET total_players='"+x+"' WHERE id = '1'");
                st.executeUpdate("UPDATE "+plugin.information_tbl+" SET newest_player='"+p.getName()+"' WHERE id = '1'");
                
            	st.executeUpdate(sql); 
            }


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
		
	}

	public void addAlias(UUID uid, String name){

		try {
			
			rs = st.executeQuery("SELECT alias_names FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
			
			st.executeUpdate("UPDATE "+plugin.players_tbl+" SET alias_names='"+rs.getString(1)+", "+name+"' WHERE UUID = '"+uid+"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public boolean checkAlias(UUID uid, String name){
		

        try {
           
            rs = st.executeQuery("SELECT alias_names FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	if(rs.getString(1).contains(name)){
            		return true;
            	}else{
            		return false;
            	}
            }else{
            	return true;
            }


        } catch (SQLException ex) {
            return true;

        }
		
	}
	
	public boolean checkNewPlayer(UUID uid, String name){
		

        try {
           
            rs = st.executeQuery("SELECT id FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return false;
            }else{
            	rs = st.executeQuery("SELECT UUID FROM "+plugin.players_tbl+" WHERE username = '"+name+"'");
            	
            	if(rs.next()){
            		st.executeUpdate("UPDATE "+plugin.players_tbl+" SET UUID='"+uid+"' WHERE username = '"+name+"'");
            		
            		return false;
            	}else{
            		return true;
            	}
            	
            	
            	
            }


        } catch (SQLException ex) {
            return true;

        }
		
	}
	
	public int totalPlayers(){
		

        try {
           
            rs = st.executeQuery("SELECT total_players FROM "+plugin.information_tbl+" WHERE id = '1'");
            
            if (rs.next()) {
            	return Integer.parseInt(rs.getString(1));
            }else{
            	return 0;
            }


        } catch (SQLException ex) {
            return 0;

        }
		
	}
	
	public String newestPlayer(){
		

        try {
           
            rs = st.executeQuery("SELECT newest_player FROM "+plugin.information_tbl+" WHERE id = '1'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown";
            }


        } catch (SQLException ex) {
            return "Unknown";

        }
		
	}
	
	public UUID getUID(String name){
		

        try {
           
            rs = st.executeQuery("SELECT UUID FROM "+plugin.players_tbl+" WHERE username LIKE '%"+name+"%' LIMIT 1");
            
            if (rs.next()) {
            	return UUID.fromString(rs.getString(1));
            }else{
            	return null;
            }


        } catch (SQLException ex) {
            return null;

        }
		
	}
	
	public String getAliasNames(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT alias_names FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Error";
            }


        } catch (SQLException ex) {
            return "Error";

        }
		
	}
	
	public String getDate(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT date_joined FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Date";
            }


        } catch (SQLException ex) {
            return "Unknown Date";

        }
		
	}
	
	public String getTime(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT time_joined FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Time";
            }


        } catch (SQLException ex) {
            return "Unknown Time";

        }
		
	}
	
	public String getPlayerNumber(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT player_number FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Player Number";
            }


        } catch (SQLException ex) {
            return "Unknown Player Number";

        }
		
	}
	
	public String getIP(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT ip_address FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown ip";
            }


        } catch (SQLException ex) {
            return "Unknown ip";

        }
		
	}
	
	public String lastDate(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT last_seen_date FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Date";
            }


        } catch (SQLException ex) {
            return "Unknown Date";

        }
		
	}
	
	public String lastTime(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT last_seen_time FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Time";
            }


        } catch (SQLException ex) {
            return "Unknown Time";

        }
		
	}
	
	public String health(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT health FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Health";
            }


        } catch (SQLException ex) {
            return "Unknown Health";

        }
		
	}
	
	public String hunger(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT hunger FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Hunger";
            }


        } catch (SQLException ex) {
            return "Unknown Hunger";

        }
		
	}
	
	public String itemInHand(UUID uid){
		

        try {
           
            rs = st.executeQuery("SELECT item_in_hand FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	return rs.getString(1);
            }else{
            	return "Unknown Item";
            }


        } catch (SQLException ex) {
            return "Unknown Item";

        }
		
	}
	
	public String location(UUID uid){
		
		String loc = "";

        try {
           
            rs = st.executeQuery("SELECT location_x FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	loc = "X: "+rs.getString(1);
            }else{
            	return "Unknown LOC";
            }
            rs = st.executeQuery("SELECT location_y FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	loc = loc+", Y: "+rs.getString(1);
            }else{
            	return "Unknown LOC";
            }

            rs = st.executeQuery("SELECT location_z FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	loc = loc+", Z: "+rs.getString(1);
            }else{
            	return "Unknown LOC";
            }


            return loc;
        } catch (SQLException ex) {
            return "Unknown Time";

        }
		
	}
	
	public void updateStats(UUID uid, String hunger, String health, int x, int y, int z, String iteminhand, Date login, Date logout){
		

        try {
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET health='"+health+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET hunger='"+hunger+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET location_x='"+x+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET location_y='"+y+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET location_z='"+z+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET item_in_hand='"+iteminhand+"' WHERE UUID = '"+uid+"'");
        	
        	
        	Date dNow = new Date( );
            SimpleDateFormat ft = 
            new SimpleDateFormat ("yyyy-MM-dd");
            
            SimpleDateFormat ft1 = 
            new SimpleDateFormat ("h:mm aa");
           
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET last_seen_date='"+ft.format(dNow)+"' WHERE UUID = '"+uid+"'");
        	
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET last_seen_time='"+ft1.format(dNow)+"' WHERE UUID = '"+uid+"'");
        	
        	
        	long diffInSeconds = (logout.getTime() - login.getTime()) / 1000;
        	
        	
        	int day = 0;
    	    int hour = 0;
    	    int minute = 0;
    	    int second = 0;
    	    
    	    
            rs = st.executeQuery("SELECT played_days FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	day = Integer.parseInt(rs.getString(1));
            }
    	    
            rs = st.executeQuery("SELECT played_hours FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	hour = Integer.parseInt(rs.getString(1));
            }
    	    
            
            rs = st.executeQuery("SELECT played_minutes FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	minute = Integer.parseInt(rs.getString(1));
            }
            
            rs = st.executeQuery("SELECT played_seconds FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	second = Integer.parseInt(rs.getString(1));
            }
    	    
    	    
            
            
    	    int day2hour = day*24;
    	    hour = day2hour+hour;
    	    int hour2minute = hour*60;
    	    minute = hour2minute+minute;
    	    int min2sec = minute*60;
    	    second = min2sec+second;
    	    
    	    diffInSeconds = diffInSeconds+second;
    	    
    	    
    	    long diff[] = new long[] { 0, 0, 0, 0 };
    	    /* sec */diff[3] = (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds);
    	    /* min */diff[2] = (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds;
    	    /* hours */diff[1] = (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds;
    	    /* days */diff[0] = (diffInSeconds = (diffInSeconds / 24));

    	    
    	    
        	
        	
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET played_days='"+diff[0]+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET played_hours='"+diff[1]+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET played_minutes='"+diff[2]+"' WHERE UUID = '"+uid+"'");
        	st.executeUpdate("UPDATE "+plugin.players_tbl+" SET played_seconds='"+diff[3]+"' WHERE UUID = '"+uid+"'");
        	
        	

        } catch (SQLException ex) {

        }
		
	}
	
	public String getPlayed(UUID uid, String days, String hours, String minutes, String seconds){
		
		String played = "";

        try {
           
            rs = st.executeQuery("SELECT played_days FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	played = rs.getString(1)+days;
            }else{
            	return "Error";
            }
            
            rs = st.executeQuery("SELECT played_hours FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	played = played+rs.getString(1)+hours;
            }else{
            	return "Error";
            }
            
            rs = st.executeQuery("SELECT played_minutes FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	played = played+rs.getString(1)+minutes;
            }else{
            	return "Error";
            }
            
            rs = st.executeQuery("SELECT played_seconds FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	played = played+rs.getString(1)+seconds;
            }else{
            	return "Error";
            }
            

            return played;

        } catch (SQLException ex) {
        	return "Error";

        }
		
	}
	
	public String getExactPlayed(UUID uid, String days, String hours, String minutes, String seconds, Date login, Date time){
		
		String played = "";

        try {
        	
        	
        	long diffInSeconds = (time.getTime() - login.getTime()) / 1000;
        	
        	
        	int day = 0;
    	    int hour = 0;
    	    int minute = 0;
    	    int second = 0;
    	    
    	    
            rs = st.executeQuery("SELECT played_days FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	day = Integer.parseInt(rs.getString(1));
            }
    	    
            rs = st.executeQuery("SELECT played_hours FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	hour = Integer.parseInt(rs.getString(1));
            }
    	    
            
            rs = st.executeQuery("SELECT played_minutes FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	minute = Integer.parseInt(rs.getString(1));
            }
            
            rs = st.executeQuery("SELECT played_seconds FROM "+plugin.players_tbl+" WHERE UUID = '"+uid+"'");
            
            if (rs.next()) {
            	second = Integer.parseInt(rs.getString(1));
            }
    	    
    	    
            
            
    	    int day2hour = day*24;
    	    hour = day2hour+hour;
    	    int hour2minute = hour*60;
    	    minute = hour2minute+minute;
    	    int min2sec = minute*60;
    	    second = min2sec+second;
    	    
    	    diffInSeconds = diffInSeconds+second;
    	    
    	    
    	    long diff[] = new long[] { 0, 0, 0, 0 };
    	    /* sec */diff[3] = (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds);
    	    /* min */diff[2] = (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds;
    	    /* hours */diff[1] = (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds;
    	    /* days */diff[0] = (diffInSeconds = (diffInSeconds / 24));
    	    
    	    played = diff[0]+days+diff[1]+hours+diff[2]+minutes+diff[3]+seconds;
    	    

            return played;

        } catch (SQLException ex) {
        	return "Error";

        }
		
	}
	
	public void addOnlinePlayer(){
		

        try {
           
            rs = st.executeQuery("SELECT online_players FROM "+plugin.information_tbl+" WHERE id = '1'");
            
            if (rs.next()) {
            	
            	int x = Integer.parseInt(rs.getString(1));
            	
            	int y = x+1;
            	
            	st.executeUpdate("UPDATE "+plugin.information_tbl+" SET online_players = '"+y+"' WHERE id = 1");
            	
            }else{
            	System.out.println("[LegendaryMessages] Database Error!!!");
            }


        } catch (SQLException ex) {
        	System.out.println("[LegendaryMessages] Database Error!!!");

        }
		
	}
	
	public void delOnlinePlayer(){
		

        try {
           
            rs = st.executeQuery("SELECT online_players FROM "+plugin.information_tbl+" WHERE id = '1'");
            
            if (rs.next()) {
            	
            	int x = Integer.parseInt(rs.getString(1));
            	
            	int y = x-1;
            	
            	st.executeUpdate("UPDATE "+plugin.information_tbl+" SET online_players = '"+y+"' WHERE id = 1");
            	
            }else{
            	System.out.println("[LegendaryMessages] Database Error!!!");
            }


        } catch (SQLException ex) {
        	
        	System.out.println("[LegendaryMessages] Database Error!!!");
        }
		
	}
	
	public void shutdown(String db){
		try {
			
			st.executeUpdate("UPDATE "+plugin.information_tbl+" SET online_players = '0' WHERE id = 1");
			System.out.println("[LegendaryMessages] Disconnected from database '"+db+"'");
			con.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
